#include "engine.h"
#include <stdlib.h>
#include <time.h>

int main(int argc, char* argv[]) {
    srand(time(NULL));
    
    struct GameInfoST *game = generate_panel(80, 60);
    if (!game) {
        return 1;
    }

    struct ProcessEngine *engine = create_new_process_engine(800, 600, 100, game);
    if (!engine) {
        panel_destroy(game);
        return 2;
    }

    while (engine_step(engine) == 0) {
    }

    engine_destroy(engine);
    panel_destroy(game);

    return 0;
}

