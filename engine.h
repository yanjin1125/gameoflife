#ifndef _GOLIFE_ENGINE_H
#define _GOLIFE_ENGINE_H

struct PanelInfo;

struct PanelInfo *create_panel(int width, int height);

void panel_destory(struct PanelInfo *f);

struct PanelInfo *panel_save_copy(struct PanelInfo *other);

int adjust_panel_size(struct PanelInfo *f);

int get_width(struct PanelInfo *f);

int get_height(struct PanelInfo *f);

int get_cell(struct PanelInfo *f, int x, int y);

void set_cell(struct PanelInfo *f, int x, int y, int val);

int count_alive_cells_around(struct PanelInfo *f, int x, int y);

struct GameInfoST;

typedef int Cell;

#define CELL_IS_DEAD  0
#define CELL_IS_ALIVE 1

struct GameInfoST *generate_panel(int width, int height);

void panel_destroy(struct GameInfoST *g);

int get_panel_width(struct GameInfoST *g);

int get_panel_height(struct GameInfoST *g);

int process(struct GameInfoST *g);

Cell get_cell_by_coord(struct GameInfoST *g, int x, int y);

void cell_spawn(struct GameInfoST *g, int x, int y);

void cell_killed(struct GameInfoST *g, int x, int y);

#define GAME_OVER 255

struct ProcessEngine;

struct ProcessEngine *create_new_process_engine(int width, int height, int delay, struct GameInfoST *game);

void engine_destroy(struct ProcessEngine *e);

int engine_step(struct ProcessEngine *e);

#endif  // _GOLIFE_ENGINE_H

