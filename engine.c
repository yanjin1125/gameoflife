#include "engine.h"
#include <time.h>
#include <SDL2/SDL.h>
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

struct ProcessEngine {
    int width;
    int height;
    int delay;
    int done;
    int paused;
    int focused_x;
    int focused_y;
    struct GameInfoST *game;

    SDL_Window *sdl_window;
    SDL_Renderer *sdl_renderer;

    Uint32 last_time_rec;
};


struct GameInfoST {
    struct PanelInfo *panel;
};


struct PanelInfo {
    int *grid; // pointer to the size of width * height
    int width;
    int height;
};

int *grid_new(int width, int height) {
    int panel_size = width * height * sizeof(int);
    int *grid = malloc(panel_size);
    if (!grid) {
        fprintf(stderr, "grid::malloc failed\n");
        return NULL;
    }

    memset(grid, 0, panel_size); // initialize the grid to zero
    return grid;
}

void grid_destroy(int *grid) {
    free(grid);
}

struct PanelInfo *create_panel(int width, int height) {
    struct PanelInfo *f = malloc(sizeof(struct PanelInfo));
    if (!f) {
        fprintf(stderr, "panel::malloc failed\n");
        return NULL;
    }

    f->grid = grid_new(width, height);
    if (!f->grid) {
        free(f);
        return NULL;
    }

    f->width = width;
    f->height = height;
    return f;
}

void panel_destory(struct PanelInfo *f) {
    if (f) {
        grid_destroy(f->grid);
        free(f);
    }
}

struct PanelInfo *panel_save_copy(struct PanelInfo *other) {
    struct PanelInfo *f = create_panel(other->width, other->height);
    if (!f) {
        return NULL;
    }
    memcpy(f->grid, other->grid, other->width * other->height * sizeof(int));
    return f;
}

static inline int max(int x, int y) {
    return x > y ? x : y;
}

static inline int min(int x, int y) {
    return x > y ? y : x;
}

int adjust_panel_size(struct PanelInfo *f) {
    int left = f->width, top = f->height, right = -1, bottom = -1;
    for (int x = 0; x < f->width; x++) { // iterate all cells to get edge position
        for (int y = 0; y < f->height; y++) {
            if (f->grid[y * f->width + x]) {
                left = min(left, x);
                top = min(top, y);
                right = max(right, x);
                bottom = max(bottom, y);
            }
        }
    }

    int add_left = left < 2;
    int add_top = top < 2;
    int add_right = right >= f->width - 2;
    int add_bottom = bottom >= f->height - 2;

    // printf("l=%d t=%d r=%d b=%d\n", add_left, add_top, add_right, add_bottom);

    int width = f->width + add_left + add_right;
    int height = f->height + add_top + add_bottom;
    if (width == f->width && height == f->height) {
        return 0;
    }

    int *grid = grid_new(width, height);
    if (!grid) {
        return -1;
    }

    for (int x = 0; x < f->width; x++) { // copy the data of old grid
        for (int y = 0; y < f->height; y++) {
            grid[(y + add_top) * width + (x + add_left)] = f->grid[y * f->width + x];
        }
    }

    grid_destroy(f->grid);

    f->grid = grid;
    f->width = width;
    f->height = height;

    return 0;
}

int get_width(struct PanelInfo *f) {
    return f->width;
}

int get_height(struct PanelInfo *f) {
    return f->height;
}

static inline void assert_valid_pos(struct PanelInfo *f, int x, int y) {
    assert(0 <= x); assert(x < f->width);
    assert(0 <= y); assert(y < f->height);
}

int get_cell(struct PanelInfo *f, int x, int y) {
    assert_valid_pos(f, x, y);
    return f->grid[y * f->width + x];
}

void set_cell(struct PanelInfo *f, int x, int y, int val) {
    assert_valid_pos(f, x, y);
    f->grid[y * f->width + x] = val;
}

int count_alive_cells_around(struct PanelInfo *f, int x, int y) {
    int result = 0;
    for (int xx = max(0, x - 1); xx < min(f->width, x + 2); xx++) {
        for (int yy = max(0, y - 1); yy < min(f->height, y + 2); yy++) {
            result += (xx != x || yy != y) && get_cell(f, xx, yy);
        }
    }
    return result;
}

struct GameInfoST *generate_panel(int width, int height) {
    struct GameInfoST *g = malloc(sizeof(struct GameInfoST));
    if (!g) {
        fprintf(stderr, "game::malloc failed\n");
        return NULL;
    }

    g->panel = create_panel(width, height);
    if (!g->panel) {
        free(g);
        return NULL;
    }

    int alive = width * height * 0.15;
    while (alive) { // set the position of alive cell
        int x = rand() % width;
        int y = rand() % height;

        if (!get_cell_by_coord(g, x, y)) { // if the value of cell x, y is zero, create cell alive here
            cell_spawn(g, x, y);
            alive--;
        }
    }

    if (adjust_panel_size(g->panel) != 0) {
        panel_destory(g->panel);
        free(g);
        return NULL;
    }

    return g;
}

void panel_destroy(struct GameInfoST *g) {
    if (g) {
        panel_destory(g->panel);
        free(g);
    }
}

int get_panel_width(struct GameInfoST *g) {
    return get_width(g->panel);
}

int get_panel_height(struct GameInfoST *g) {
    return get_height(g->panel);
}

int process(struct GameInfoST *g) {
    struct PanelInfo *tmp_panel = panel_save_copy(g->panel);
    for (int x = 0, w = get_panel_width(g); x < w; x++) {
        for (int y = 0, h = get_panel_height(g); y < h; y++) {
            int cell = get_cell(tmp_panel, x, y);
            int num_of_alives = count_alive_cells_around(tmp_panel, x, y);
            if (cell) {
                if (num_of_alives != 2 && num_of_alives != 3) {
                    cell_killed(g, x, y);
                }
            } else {
                if (num_of_alives == 3) {
                    cell_spawn(g, x, y);
                }
            }
        }
    }
    panel_destory(tmp_panel);

    return 0;
}

Cell get_cell_by_coord(struct GameInfoST *g, int x, int y) {
    return get_cell(g->panel, x, y);
}

void cell_spawn(struct GameInfoST *g, int x, int y) {
    set_cell(g->panel, x, y, CELL_IS_ALIVE);
}

void cell_killed(struct GameInfoST *g, int x, int y) {
    set_cell(g->panel, x, y, CELL_IS_DEAD);
}

struct ProcessEngine *create_new_process_engine(int width, int height, int delay, struct GameInfoST *game) {
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        fprintf(stderr, "SDL_Init failed: %s\n", SDL_GetError());
        return NULL;
    }

    struct ProcessEngine *e = malloc(sizeof(struct ProcessEngine));
    if (!e) {
        fprintf(stderr, "engine::malloc failed\n");
        return NULL;
    }

    e->width = width;
    e->height = height;
    e->delay = delay;
    e->done = 0;
    e->paused = 0;
    e->focused_x = -1;
    e->focused_y = -1;
    e->game = game;

    e->sdl_window = NULL;
    e->sdl_renderer = NULL;

    e->last_time_rec = 0;

    if (SDL_CreateWindowAndRenderer(width, height, 0, &e->sdl_window, &e->sdl_renderer) != 0) {
        fprintf(stderr, "SDL_CreateWindowAndRenderer failed: %s\n", SDL_GetError());
        return NULL;
    }

    return e;
}

void engine_destroy(struct ProcessEngine *e) {
    if (e) {
        if (e->sdl_renderer) {
            SDL_DestroyRenderer(e->sdl_renderer);
        }
        if (e->sdl_window) {
            SDL_DestroyWindow(e->sdl_window);
        }

        SDL_Quit();
        free(e);
    }
}

static int screen_clear(struct ProcessEngine *e) {
    int err;

    // set empty block color to black 0, 0, 0
    if ((err = SDL_SetRenderDrawColor(e->sdl_renderer, 0, 0, 0, SDL_ALPHA_OPAQUE)) != 0) {
        fprintf(stderr, "SDL_SetRenderDrawColor failed: %s\n", SDL_GetError());
        return err;
    }

    // fill the screen with current drawing color `black` which is set above
    if ((err = SDL_RenderClear(e->sdl_renderer)) != 0) {
        fprintf(stderr, "SDL_RenderClear failed: %s\n", SDL_GetError());
        return err;
    }
    return 0;
}

static void screen_present(struct ProcessEngine *e) {
    SDL_RenderPresent(e->sdl_renderer);
}

static inline int calc_cell_width(struct ProcessEngine *e) {
    return e->width / get_panel_width(e->game);
}

static inline int calc_cell_height(struct ProcessEngine *e) {
    return e->height / get_panel_height(e->game);
}

static int draw_grid(struct ProcessEngine *e) {
    int err;
    // calc the block size
    int cell_width = calc_cell_width(e);
    int cell_height = calc_cell_height(e);

    // set the grid line color
    if ((err = SDL_SetRenderDrawColor(e->sdl_renderer, 128, 128, 128, SDL_ALPHA_OPAQUE)) != 0) {
        fprintf(stderr, "SDL_SetRenderDrawColor failed: %s\n", SDL_GetError());
        return err;
    }

    int x = 0;
    while (x < e->width) {
        if ((err = SDL_RenderDrawLine(e->sdl_renderer, x, 0, x, e->height)) != 0) {
            fprintf(stderr, "SDL_RenderDrawLine failed: %s\n", SDL_GetError());
            return err;
        }
        x += cell_width;
    }

    int y = 0;
    while (y < e->height) {
        if ((err = SDL_RenderDrawLine(e->sdl_renderer, 0, y, e->width, y)) != 0) {
            fprintf(stderr, "SDL_RenderDrawLine failed: %s\n", SDL_GetError());
            return err;
        }
        y += cell_height;
    }

    return 0;
}

static int draw_cells(struct ProcessEngine *e) {
    int err;
    int cell_width = calc_cell_width(e);
    int margin_left = cell_width / 8;
    int cell_height = calc_cell_height(e);
    int margin_top = cell_height / 8;
    SDL_Rect cell_rect;

    if ((err = SDL_SetRenderDrawBlendMode(e->sdl_renderer, SDL_BLENDMODE_BLEND)) != 0) {
        fprintf(stderr, "SDL_SetRenderDrawBlendMode failed: %s\n", SDL_GetError());
        return err;
    }

    for (int x = 0, gw = get_panel_width(e->game); x < gw; x++) {
        for (int y = 0, gh = get_panel_height(e->game); y < gh; y++) {
            Cell cell = get_cell_by_coord(e->game, x, y);
            if (cell) {
                if ((err = SDL_SetRenderDrawColor(e->sdl_renderer, 0, 255, 0, 127)) != 0) {
                    fprintf(stderr, "SDL_SetRenderDrawColor failed: %s\n", SDL_GetError());
                    return err;
                }

                cell_rect.x = x * cell_width + margin_left;
                cell_rect.y = y * cell_height + margin_top;
                cell_rect.w = cell_width - 2 * margin_left;
                cell_rect.h = cell_height - 2 * margin_top;

                if ((err = SDL_RenderFillRect(e->sdl_renderer, &cell_rect)) != 0) {
                    fprintf(stderr, "SDL_RenderFillRect failed: %s\n", SDL_GetError());
                    return err;
                }
            }

            if (x == e->focused_x && y == e->focused_y) {
                if ((err = SDL_SetRenderDrawColor(e->sdl_renderer, cell*255, !cell*255, 0, 127)) != 0) {
                    fprintf(stderr, "SDL_SetRenderDrawColor failed: %s\n", SDL_GetError());
                    return err;
                }

                cell_rect.x = x * cell_width;
                cell_rect.y = y * cell_height;
                cell_rect.w = cell_width;
                cell_rect.h = cell_height;

                if ((err = SDL_RenderFillRect(e->sdl_renderer, &cell_rect)) != 0) {
                    fprintf(stderr, "SDL_RenderFillRect failed: %s\n", SDL_GetError());
                    return err;
                }
            }
        }
    }

    return 0;
}

static int draw_scene(struct ProcessEngine *e) {
    int err;
    // clear the screen
    if ((err = screen_clear(e)) != 0) {
        fprintf(stderr, "engine::screen_clear failed: %d\n", err);
        return err;
    }

    // draw the screen
    if ((err = draw_grid(e)) != 0) {
        fprintf(stderr, "engine::draw_grid failed: %d\n", err);
        return err;
    }

    if ((err = draw_cells(e)) != 0) {
        fprintf(stderr, "engine::draw_cells failed: %d\n", err);
        return err;
    }

    // Update the screen with any rendering performed since the previous call.
    screen_present(e);
    return 0;
}

static int terminal_signal_process(struct ProcessEngine *e) {
    int err;
    SDL_Event evt;
    while (SDL_PollEvent(&evt)) {
        if (evt.type == SDL_QUIT) {
            e->done = 1;
            break;
        }
    }

    return 0;
}

int engine_step(struct ProcessEngine *e) {
    int err;
    if ((err = draw_scene(e) != 0)) {
        return err;
    }

    if (!e->paused) {
        Uint32 now = SDL_GetTicks();
        if ((now - e->last_time_rec) >= e->delay) {
            if ((err = process(e->game)) != 0) {
                fprintf(stderr, "engine::process failed: %d\n", err);
                return err;
            }
            e->last_time_rec = now;
        }
    }

    if ((err = terminal_signal_process(e)) != 0) {
        return err;
    }
    if (e->done) {
        return GAME_OVER;
    }

    return 0;
}

